var glob = require('glob');
var fs = require('fs-extra');

function clean(args){
    var paths = args.splice(2, args.length);
    var excludes = [];
    var includes = [];

    for(var i = 0; i < paths.length; i++){
        if(paths[i].indexOf('!') !== -1){
            excludes.push(paths[i].replace('!', ''))
        }
        else{
            includes.push(paths[i]);
        }
    }

    for(i = 0; i < includes.length; i++){
        glob(includes[i], {ignore: excludes}, (err, files)=>{
            if(err) throw err;
            // console.log(files);
            files.forEach((item, index, array)=>{
                console.log(item);
                fs.remove(item);
            });
        });
    }
}

clean(process.argv);

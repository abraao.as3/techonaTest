var imagemin = require('imagemin');
var imageminMozjpeg = require('imagemin-mozjpeg');
var imageminPngquant = require('imagemin-pngquant');
var glob = require('glob');

var list = [];
var count = 0;

glob('src/**/*.{png,jpg}', (err, files)=>{

    list = files;

        optimize();

});

function optimize(){
    if(count >= list.length) return;

    receive(list[count]);

    count++;
}

function receive(item){
    var saida = 'dist' + item.replace(/^src|([\w _ - \.]+\.\w{3})$/g, '');

    if(item.indexOf('.png') !== -1){
        minPng(item, saida);
    }

    if(item.indexOf('.jpg') !== -1 || item.indexOf('.jpeg') !== -1){
        minJpg(item, saida);
    }
}

function minPng(entrada, saida){
    imagemin([entrada], saida, {
        use: [
            imageminPngquant({quality: "60-60"})
        ]
    }).then(files=>{
        console.log(files[0].path);
        optimize();
    });
}

function minJpg(entrada, saida){
    console.log(entrada);
    imagemin([entrada], saida, {
        use: [
            imageminMozjpeg({quality: 60})
        ]
    }).then(files=>{
        console.log(files[0].path);
        optimize();
    });
}

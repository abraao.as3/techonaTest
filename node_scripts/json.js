var fs = require('fs-extra');
var glob = require('glob');

glob('src/**/*.json', {}, (err, files)=>{
    files.forEach(receive);
});

function receive(item, index, array){
    var saida = 'dist' + item.replace(/^src|([\w _ - \.]+\.\w{3})$/g, '');
    min(item, saida);
}

function min(entrada, saida){
    var _json = fs.readJsonSync(entrada);
    fs.outputJsonSync(saida, _json);
}

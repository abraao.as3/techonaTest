var fs = require('fs');
var fse = require('fs-extra');

var exec = require('child_process').exec;

var fila = [];

fs.watch("src/", {
    'recursive': true
}, (eventType, filename) => {

    var add = true;

    console.log(filename);

    if (filename.indexOf(".js") !== -1) {

        for (var i = 0; i < fila.length; i++) {

            if (fila[i] == filename) {
                add = false
            }
        }

        if (add) {
            fila.push(filename);
        }

    }
});

var webpackexecuted = false;
var loading = false;
var txtLoading = ".";

var web_count = 0;

setInterval(() => {
    //realiza os itens da fila
    webpackexecuted = false;
    loading = false;

    // console.log(fila);

    if(fila.length > 0){
        if(loading === false){console.log("\nLoading..."); loading=true;}
        else{console.log("\n"+txtLoading); txtLoading = txtLoading.concat(".");}
    }

    for (var i = 0; i < fila.length; i++) {

        if (!webpackexecuted && fila[i].indexOf(".js") !== -1)
            {

            exec("webpack", (error, stdout, stderr) => {
                console.log(stdout);
                var data = new Date();
                console.log("webpack updated >>> id:", web_count, "| time:", data.getHours(),":",data.getMinutes(),":",data.getSeconds());
                web_count+=1;
            });
            webpackexecuted = true;

        }
    }

    fila.splice(0);

}, 1000);

import $ from 'jquery';
import Game from './Game';

$(()=>{
    var body = $('body');
    body.css({
        margin: 0,
        width: '100%',
        minHeight: '100%',
        height: '100vh',
        backgroundColor: '#f9c667'
    });
    body.css('background-image', '-moz-linear-gradient(top, #f9c667 0%, #d39400 100%)');
    body.css('background-image', '-webkit-linear-gradient(top, #f9c667 0%,#d39400 100%)');
    body.css('background-image', 'linear-gradient(to bottom, #f9c667 0%,#d39400 100%)');
    body.css('filter', "progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#d39400',GradientType=0 )");

    var game = new Game();
    game.start();

    function resize(){
        game.resize();
    }

    $(window).on('resize', resize);
    resize();
});

require('pixi.js');
import autobind from 'autobind-decorator';
import TweenMax from 'gsap';

import Assets from '../controls/Assets';

@autobind
class Cup extends PIXI.Container{
    @Private _assets = null;
    @Private _cup = null;
    @Private _shadow = null;
    @Private _isOpen = false;
    @Private _id = null;

    constructor(){
        super();

        this._assets = Assets.getInstance();

        this._cup = new PIXI.Sprite(this._assets.getTexture('cup'));
        this.addChild(this._cup);

        this._shadow = new PIXI.Sprite(this._assets.getTexture('cupShadow'));
        this.addChildAt(this._shadow, 0);

        this._shadow.x = this._cup.width / 2 - this._shadow.width / 2;
        this._shadow.y = this._cup.height - this._shadow.height;

        this._shadow.alpha = 0;
    }

    @Private
    _onDown(){

    }

    @Private
    _onUp(){
        this.emit('clicked', {currentTarget: this});
        if(this._isOpen){
            this.close();
        }
        else{
            this.open();
        }
    }

    open(){
        TweenMax.to(this._cup, 0.3, {y: -50});
        TweenMax.to(this._shadow, 0.3, {alpha: 0.5});

        this._isOpen = true;
    }

    close(){
        TweenMax.to(this._cup, 0.3, {y: 0});
        TweenMax.to(this._shadow, 0.3, {alpha: 0});

        this._isOpen = false;
    }

    addEvents(){
        this._cup.interactive = true;
        this._cup.buttonMode = true;

        this._cup.on('mousedown', this._onDown);
        this._cup.on('touchstart', this._onDown);

        this._cup.on('mouseup', this._onUp);
        this._cup.on('touchend', this._onUp);
        this._cup.on('mouseupoutside', this._onUp);
        this._cup.on('touchendoutside', this._onUp);
    }

    removeEvents(){
        this._cup.interactive = false;
        this._cup.buttonMode = false;

        this._cup.removeListener('mousedown');
        this._cup.removeListener('touchstart');

        this._cup.removeListener('mouseup');
        this._cup.removeListener('touchend');
        this._cup.removeListener('mouseupoutside');
        this._cup.removeListener('touchendoutside');
    }

    get id(){
        return this._id;
    }

    set id(value){
        this._id = {} = value;
    }
}

export default Cup;

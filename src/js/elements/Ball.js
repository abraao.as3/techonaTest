require('pixi.js');
import autobind from 'autobind-decorator';
import TweenMax from 'gsap';

import Assets from '../controls/Assets';

@autobind
class Ball extends PIXI.Container{
    @Private _assets = null;
    @Private _ball = null;

    constructor(){
        super();

        this._assets = Assets.getInstance();

        this._ball = new PIXI.Sprite(this._assets.getTexture("ball"));
        this._ball.pivot.set(this._ball.width / 2, this._ball.height / 2);
        this.addChild(this._ball);
    }

    animation(){
        this._ball.rotation = 0;
        TweenMax.to(this._ball, 1, {rotation: Math.PI * 2, repeat: -1, ease: Linear.easeNone});
    }

    stopAnimation(){
        TweenMax.killTweensOf(this._ball);
    }
}

export default Ball;

require('pixi.js');
import autobind from 'autobind-decorator';

import Const from '../utils/Const';
import Assets from '../controls/Assets';

import Cup from '../elements/Cup';
import Ball from '../elements/Ball';

@autobind
class GamePlay extends PIXI.Container{
    @Private _assets = null;
    @Private table = null;
    @Private _cups = [];

    @Private _initialized = false;

    @Private _cupY = 450;
    @Private _cupX = 50;

    @Private ball = null;

    @Private ballInitX = Const.BASE_WIDTH / 2;
    @Private ballInitY = 650;

    @Private _answer = null;

    constructor(){
        super();

        this._assets = Assets.getInstance();
        if(this._assets.isLoaded){
            this._init();
        }
        else{
            this._assets.once('loadComplete', this._init, this);
        }

        this.alpha = 0;
    }

    @Private
    _init(){
        this.table = new PIXI.Sprite(this._assets.getTexture('table'));
        this.addChild(this.table);

        this.table.y = Const.BASE_HEIGHT - this.table.height;

        this.ball = new Ball();
        this.addChild(this.ball);

        this.ball.x = {} = this.ballInitX;
        this.ball.y = {} = this.ballInitY;

        for(var i = 0; i < 3; i++){
            var cup = new Cup();
            cup.id = {} = i;
            this.addChild(cup);
            this._cups.push(cup);

            cup.x = {} = this._cupX + (cup.width + 15) * i;
            cup.y = {} = this._cupY;
        }

        this._initialized = true;
    }

    @Private
    _initialShoot(){
        var rand = this._getRandomInt();
        var cup = this._cups[rand];
        TweenMax.to(this.ball, 1,
            {
                x: cup.x + cup.width / 2,
                y: cup.y + cup.height - this.ball.height + 10,
                onComplete: ()=>{
                    this.ball.stopAnimation();
                    cup.close();
                    TweenMax.to(this.ball, 0.5,
                        {
                            alpha: 0,
                            onComplete: ()=>{
                                this._cupsAnimation();
                            },
                            callbackScope: this
                        });
                },
                callbackScope: this
            });
        this.ball.animation();
        cup.open();
    }

    @Private
    _cupsAnimation(){
        var c0 = this._cups[0];
        var c1 = this._cups[1];
        var c2 = this._cups[2];

        var tl = new TimelineMax(
            {
                repeat: 3,
                onRepeat: ()=>{
                    this.setChildIndex(c2, this.children.length - 1);
                },
                onComplete: ()=>{
                    tl.paused(true);
                    tl.seek(0);
                    tl.kill();
                    tl = null;
                    this._initInteractions();
                },
                callbackScope: this
            });

        this.setChildIndex(c0, 1);

        tl.to(c0, 3,{
            bezier:  {
                type: "soft",
                values: [{x: c0.x + (c0.width + 15) / 2, y: c0.y - 50}, {x: c0.x + (c0.width + 15), y: c0.y}]
            },
            ease: Linear.easeNone
        }, 0);

        tl.to(c1, 3,{
            bezier:  {
                type: "soft",
                values: [{x: c1.x + (c1.width + 15) / 2, y: c1.y + 50}, {x: c1.x + (c1.width + 15), y: c1.y}]
            },
            onStart: ()=>{
                this.setChildIndex(c1, this.children.length - 1);
            },
            callbackScope: this,
            ease: Linear.easeNone
        }, 0);

        tl.to(c2, 1.5,{
            bezier:  {
                type: "soft",
                values: [{x: c2.x - (c2.width + 15) / 2, y: c2.y - 50}, {x: c2.x - (c2.width + 15), y: c2.y}]
            },
            ease: Linear.easeNone
        }, 0);

        tl.to(c2, 1.5,{
            bezier:  {
                type: "soft",
                values: [{x: c2.x - (c2.width + 15), y: c2.y + 50}, {x: c2.x - (c2.width + 15) * 2, y: c2.y}]
            },
            ease: Linear.easeNone
        }, 1.5);

        tl.to(c0, 3,{
            bezier:  {
                type: "soft",
                values: [{x: c0.x + (c0.width + 15), y: c0.y + 50}, {x: c0.x + (c0.width + 15) * 2, y: c0.y}]
            },
            onStart: ()=>{
                this.setChildIndex(c0, this.children.length - 1);
            },
            callbackScope: this,
            ease: Linear.easeNone
        }, 3);

        tl.to(c1, 2,{
            bezier:  {
                type: "soft",
                values: [{x: c1.x + (c1.width + 15) / 2, y: c1.y - 50}, {x: c1.x, y: c1.y}]
            },
            ease: Linear.easeNone
        }, 3);

        tl.timeScale(3);

    }

    @Private
    _getRandomInt(){
        return Math.round(Math.random() * 2);
    }

    @Private
    _initInteractions(){
        this._answer = this._getRandomInt();
        console.log(this._answer);

        for(var i = 0; i < this._cups.length; i++){
            var c = this._cups[i];
            c.addEvents();
            c.once('clicked', this._checkAnswer, this);
        }
    }

    @Private
    _checkAnswer(e){
        for(var i = 0; i < this._cups.length; i++){
            var c = this._cups[i];
            c.removeEvents();
        }

        var cup = e.currentTarget;

        if(this._answer == cup.id){
            this.ball.x = cup.x + cup.width / 2;
            this.ball.y = cup.y + cup.height - this.ball.height + 10;
            TweenMax.to(this.ball, 0.3, {alpha: 1});
            this.emit('success');
        }
        else{
            setTimeout(()=>{
                cup.close();
                var correct = this._cups[this._answer];
                this.ball.x = correct.x + correct.width / 2;
                this.ball.y = correct.y + correct.height - this.ball.height + 10;
                setTimeout(()=>{
                    correct.open();
                    TweenMax.to(this.ball, 0.3, {alpha: 1});
                    this.emit('fail');
                }, 500);
            }, 1000);
        }
    }

    start(){
        if(!this._initialized){
            var _enterframe = setInterval(()=>{
                if(this._initialized){
                    clearInterval(_enterframe);
                    this.start();
                }
            }, 1000 / 60);
            return;
        }

        TweenMax.to(this, 0.5, {alpha: 1, onComplete: ()=>{
            setTimeout(()=>{
                this._initialShoot();
            }, 1000);
        }, callbackScope: this});
    }

    close(){

    }

    reset(){
        this.ball.animation();
        TweenMax.to(this.ball, 0.5, {
            x: this.ballInitX,
            y: this.ballInitY,
            onComplete: ()=>{
                this.ball.stopAnimation();

                setTimeout(()=>{
                    this.emit('resetComplete');
                }, 500);
            },
            callbackScope: this
        });

        for(var i = 0; i < this._cups.length; i ++){
            this._cups[i].close();
        }
    }

}

export default GamePlay;

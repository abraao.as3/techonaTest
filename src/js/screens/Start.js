require('pixi.js');
import autobind from 'autobind-decorator';
import TweenMax from 'gsap';

import Assets from '../controls/Assets';
import Const from '../utils/Const';

@autobind
class Start extends PIXI.Container{
    @Private _assets = null;
    @Private _initialized = false;
    @Private _logo = null;
    @Private _play = null;

    constructor(){
        super();

        this._assets = Assets.getInstance();
        if(this._assets.isLoaded){
            this._init();
        }
        else{
            this._assets.once('loadComplete', this._init, this);
        }
    }

    @Private
    _init(){
        this._logo = new PIXI.Sprite(this._assets.getTexture('logo'));
        this.addChild(this._logo);

        this._logo.x = Const.BASE_WIDTH / 2 - this._logo.width / 2;
        this._logo.y = Const.BASE_HEIGHT / 2 - this._logo.height / 2;

        this._play = new PIXI.Sprite(this._assets.getTexture('play_bt'));
        this.addChild(this._play);

        this._play.pivot.set(this._play.width / 2, this._play.height / 2);
        this._play.x = Const.BASE_WIDTH / 2;
        this._play.y = Const.BASE_HEIGHT / 2;

        TweenMax.to([this._logo, this._play], 0, {alpha: 0});

        this._initialized = true;
    }

    @Private
    _initInteractions(){
        this._addEvents();
    }

    @Private
    _addEvents(){
        this._play.interactive = true;
        this._play.buttonMode = true;

        this._play.on('mousedown', this._onDown);
        this._play.on('touchstart', this._onDown);

        this._play.on('mouseup', this._onUp);
        this._play.on('touchend', this._onUp);
        this._play.on('mouseupoutside', this._onUp);
        this._play.on('touchendoutside', this._onUp);
    }

    @Private
    _removeEvents(){
        this._play.interactive = false;
        this._play.buttonMode = false;

        this._play.removeListener('mousedown');
        this._play.removeListener('touchstart');

        this._play.removeListener('mouseup');
        this._play.removeListener('touchend');
        this._play.removeListener('mouseupoutside');
        this._play.removeListener('touchendoutside');
    }

    @Private
    _onDown(){
        this._play.scale.set(0.95);
    }

    @Private
    _onUp(){
        this._play.scale.set(1);
        this.emit('clicked');
    }

    start(){
        if(!this._initialized){
            var _enterframe = setInterval(()=>{
                if(this._initialized){
                    clearInterval(_enterframe);
                    this.start();
                }
            }, 1000 / 60);
            return;
        }
        var tl = new TimelineMax({onComplete: ()=>{
            tl = null;
            this._initInteractions();
        }, callbackScope: this});

        tl.to(this._logo, 1, {alpha: 1, yoyo: true, repeat: 1, repeatDelay: 3});
        tl.to(this._play, 0.5, {alpha: 1}, '+=1');
    }

    close(){
        this._removeEvents();
        TweenMax.to(this._play, 0.3, {alpha: 0, onComplete: ()=>{
            this.emit('closed');
        }, callbackScope: this});
    }
}

export default Start;

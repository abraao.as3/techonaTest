require('pixi.js');
import autobind from 'autobind-decorator';
import TweenMax from 'gsap';

import Assets from '../controls/Assets';
import Const from '../utils/Const';

@autobind
class End extends PIXI.Container{

    @Private _assets = null;
    @Private _successMensage = null;
    @Private _failMensage = null;
    @Private _resetButton = null;

    constructor(){
        super();

        this._assets = Assets.getInstance();

        this._assets = Assets.getInstance();
        if(this._assets.isLoaded){
            this._init();
        }
        else{
            this._assets.once('loadComplete', this._init, this);
        }
    }

    @Private
    _init(){
        this._successMensage = new PIXI.Sprite(this._assets.getTexture('congratulations'));
        this._failMensage = new PIXI.Sprite(this._assets.getTexture('gameOver'));
        this._resetButton = new PIXI.Sprite(this._assets.getTexture('restart'));

        this._successMensage.pivot.set(this._successMensage.width / 2, this._successMensage.height / 2);
        this._failMensage.pivot.set(this._failMensage.width / 2, this._failMensage.height / 2);


        this._resetButton.pivot.set(this._resetButton.width / 2, this._resetButton.width / 2);
        this._resetButton.x = Const.BASE_WIDTH / 2;
        this._resetButton.y = Const.BASE_HEIGHT / 2 - 100;

        this.addChild(this._resetButton);
    }

    @Private
    _addEvents(){
        this._resetButton.interactive = true;
        this._resetButton.buttonMode = true;

        this._resetButton.on('mousedown', this._onDown);
        this._resetButton.on('touchstart', this._onDown);

        this._resetButton.on('mouseup', this._onUp);
        this._resetButton.on('touchend', this._onUp);
        this._resetButton.on('mouseupoutside', this._onUp);
        this._resetButton.on('touchendoutside', this._onUp);
    }

    @Private
    _removeEvents(){
        this._resetButton.interactive = false;
        this._resetButton.buttonMode = false;

        this._resetButton.removeListener('mousedown');
        this._resetButton.removeListener('touchstart');

        this._resetButton.removeListener('mouseup');
        this._resetButton.removeListener('touchend');
        this._resetButton.removeListener('mouseupoutside');
        this._resetButton.removeListener('touchendoutside');
    }

    @Private
    _onDown(){
        this._resetButton.scale.set(0.95);
    }

    @Private
    _onUp(){
        this._removeEvents();
        this._resetButton.scale.set(1);
        this.emit('reset');
    }

    @Private
    _showResetButton(){
        TweenMax.to(this._resetButton, 0.3, {alpha: 1, onComplete: ()=>{
            this._addEvents();
        }, callbackScope: this});
    }

    start(status){
        this._resetButton.alpha = 0;
        if(this.children.length > 1){
            this.removeChildAt(0);
        }

        this.alpha = 1;

        if(status == "success"){
            this.addChildAt(this._successMensage, 0);
            this._successMensage.alpha = 0;

            this._successMensage.x = Const.BASE_WIDTH / 2;
            this._successMensage.y = this._successMensage.height / 2;

            TweenMax.from(this._successMensage.scale, 1,
                {
                    x: 2,
                    y: 2,
                    delay: 1,
                    ease: Bounce.easeOut,
                    onStart: ()=>{
                        this._successMensage.alpha = 1;
                    },
                    onComplete: ()=>{
                        this._showResetButton();
                    },
                    callbackScope: this
                }
            );
        }
        else if(status == "fail"){
            this.addChildAt(this._failMensage, 0);

            this._failMensage.x = Const.BASE_WIDTH / 2;
            this._failMensage.y = this._failMensage.height / 2;

            TweenMax.from(this._failMensage.scale, 1,
                {
                    x: 0,
                    y: 0,
                    delay: 1,
                    ease: Back.easeOut,
                    onComplete: ()=>{
                        this._showResetButton();
                    },
                    callbackScope: this
                }
            );
        }
        else{
            return;
        }
    }

    close(){
        this._removeEvents();
        TweenMax.to(this, 0.3, {alpha: 0, onComplete: ()=>{
            if(this.parent){
                var p = this.parent;
                p.removeChild(this);
            }
        }, callbackScope: this});
    }
}

export default End;

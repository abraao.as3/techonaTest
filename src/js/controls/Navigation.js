require('pixi.js');
import autobind from 'autobind-decorator';
import TweenMax from 'gsap';

import Const from '../utils/Const';
import Start from '../screens/Start';
import GamePlay from '../screens/GamePlay';
import End from '../screens/End';

@autobind
class Navigation extends PIXI.Container{
    @Private _start = null;
    @Private _gameplay = null;
    @Private _end = null;
    @Private _currentScreen = null;

    constructor(){
        super();

        this._start = new Start();
        this._gameplay = new GamePlay();
        this._end = new End();
    }

    @Private
    _openPage(page){
        if(this._currentScreen){
            this.once('screenClosed', ()=>{
                this._openPage(page);
            }, this);
            this._closePage(this._currentScreen);
            return;
        }

        this._currentScreen = page;
        this.addChild(this._currentScreen);
        this._currentScreen.start();
    }

    @Private
    _closePage(page){
        this._currentScreen.once('closed', ()=>{
            this.removeChild(this._currentScreen);
            this._currentScreen = null;
            this.emit('screenClosed');
        }, this);

        this._currentScreen.close();
    }

    @Private
    _initGame(){
        this._openPage(this._gameplay);
        this._gameplay.once('success', ()=>{
            this._gameplay.removeListener('fail');
            this._initEnd('success');
        }, this);

        this._gameplay.once('fail', ()=>{
            this._gameplay.removeListener('success');
            this._initEnd('fail');
        }, this);
    }

    @Private
    _initEnd(status){
        this.addChild(this._end);
        this._end.start(status);
        this._end.once('reset', this._resetGamePlay, this);
    }

    @Private
    _resetGamePlay(){
        this._currentScreen = null;
        this._end.close();

        this._gameplay.reset();
        this._gameplay.once('resetComplete', this._initGame, this);
    }

    start(){
        this._openPage(this._start);
        this._start.once('clicked', this._initGame, this);
    }
}

export default Navigation;

require('pixi.js');
import EventEmitter from 'eventemitter3';
import autobind from 'autobind-decorator';

@autobind
class Assets extends EventEmitter{

    @Private _loader = null;
    @Private _isLoaded = false;
    @Private _textures = null;

    constructor(){
        if(Assets._instance){
            throw "use Assets.getInstance()";
        }
        super();

        Assets._instance = this;

        this._loader = new PIXI.loaders.Loader();
        this._loader.add('sprite', 'sprites/sprites.json');
        this._loader.load(this._loaderComplete);
    }

    get isLoaded(){
        return this._isLoaded;
    }

    @Private
    _loaderComplete(loaderObj, resource){
        this._textures = resource.sprite.textures;
        this._isLoader = true;
        this.emit('loadComplete');
    }

    getTexture(name){
        return this._textures[name + '.png'];
    }

    static _instance = null;

    static getInstance(){
        if(!Assets._instance){
            new Assets();
        }

        return Assets._instance;
    }
}

export default Assets;

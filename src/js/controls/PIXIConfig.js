require('pixi.js');
import autobind from 'autobind-decorator';

import Const from '../utils/Const';

@autobind
class PIXIConfig{
    @Private pixi = null;
    @Private stage = null;

    @Private _factorScale = 1;

    constructor(){
        this.pixi = PIXI.autoDetectRenderer(Const.BASE_WIDTH, Const.BASE_HEIGHT,{transparent: true});

        this.pixi.view.style.position = 'absolute';
        this.pixi.view.style.width = Const.BASE_WIDTH +'px';
        this.pixi.view.style.height = Const.BASE_HEIGHT +'px';
        this.pixi.view.style.border ='1px solid white';

        this.stage = new PIXI.Container();
        document.body.appendChild(this.pixi.view);
    }

    addChild(child){
        this.stage.addChild(child);
    }

    removeChild(child){
        this.stage.removeChild(child);
    }

    update(){
        this.pixi.render(this.stage);
    }

    resize(){
        var scaleX = window.innerWidth / Const.BASE_WIDTH;
        var scaleY = window.innerHeight / Const.BASE_HEIGHT;

        var dom = this.pixi.view;

        if(scaleX <= 1){
            dom.style.width = Const.BASE_WIDTH * scaleX + 'px';
            dom.style.height = Const.BASE_HEIGHT * scaleX + 'px';


            this._factorScale = scaleX;
        }
        else{
            dom.style.width = Const.BASE_WIDTH + 'px';
            dom.style.height = Const.BASE_HEIGHT + 'px';

            this._factorScale = 1;
        }

        if (scaleY <= 1) {
            if (Const.BASE_WIDTH * scaleY <= window.innerWidth) {
                dom.style.width = Const.BASE_WIDTH * scaleY + 'px';
                dom.style.height = Const.BASE_HEIGHT * scaleY + 'px';

                this._factorScale = scaleY;
            }
        }

        dom.style.left = window.innerWidth / 2 - parseFloat(dom.style.width) / 2 + 'px';
        dom.style.top = window.innerHeight / 2 - parseFloat(dom.style.height) / 2 + 'px';
    }
}

export default PIXIConfig;

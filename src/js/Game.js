import autobind from 'autobind-decorator';
import PIXIConfig from './controls/PIXIConfig';

import Assets from './controls/Assets';
import Navigation from './controls/Navigation';

@autobind
class Game{
    @Private pixi = null;
    @Private assets = null;
    @Private navigation = null;

    constructor(){
        this.pixi = new PIXIConfig();
        this.assets = Assets.getInstance();
        this.navigation = new Navigation();
    }

    start(){
        this.update();

        this.pixi.addChild(this.navigation);
        this.navigation.start();
    }

    @Private
    update(){
        requestAnimationFrame(this.update);

        this.pixi.update();
    }

    resize(){
        this.pixi.resize();
    }
}

export default Game;

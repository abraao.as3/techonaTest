const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/js/Main.js',
    output: {
        path: path.resolve(__dirname, 'dist/js'),
        filename: 'script.js'
    },

    module:{
        rules: [
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, "src/js")
                ],
                loader: "babel-loader",
                options: {
                    presets: ['env'],
                    plugins: [
                        'babel-plugin-transform-private-properties',
                        'transform-decorators-legacy',
                        'transform-class-properties'
                    ]
                }
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
          title: 'Techona',
          inject: 'head',
          hash: true,
          filename: '../index.html',
          minify: {collapseWhitespace:true}
        })
    ],

    watch: false
}
